const initial = {
    data: [],
    loading: false,
    error: false
}

export default (state = initial, action) => {
    switch (action.type) {
        case 'GET_PRODUCT_PENDING':
            return {
                ...state,
                loading: true
            }
        case 'GET_PRODUCT_FULFILLED':
            return {
                loading: false,
                error: false,
                data: action.payload
            }
        case 'GET_PRODUCT_REJECTED':
            return {
                ...state,
                loading: false,
                error: true
            }
        default:
            return state
    }
}
