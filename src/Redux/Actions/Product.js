import Http from 'axios'

export const getProducts = () => ({
    type: 'GET_PRODUCT',
    payload: Http.get('https://sutanlab-backend-pos.herokuapp.com/api/product')
        .then(({ data }) => Promise.resolve(data.data.rows))
        .catch(err => Promise.reject(err))
})
