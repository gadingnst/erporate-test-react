import { NEWSAPI_KEY } from 'react-native-dotenv'
import Http from 'axios'

export const getArticles = () => ({
    type: 'GET_ARTICLE',
    payload: Http.get(
        `https://newsapi.org/v2/top-headlines?country=id&apiKey=${NEWSAPI_KEY}`
    )
        .then(({ data }) => Promise.resolve(data.articles))
        .catch(err => Promise.reject(err))
})
