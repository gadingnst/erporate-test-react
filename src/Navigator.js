import React from 'react'
import { Icon } from 'native-base'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { fromRight } from 'react-navigation-transitions'
import Color from './Assets/Color'

import Home from './Screens/Home'
import Product from './Screens/Product'
import Article from './Screens/Article'
import About from './Screens/About'
import DetailProduct from './Screens/DetailProduct'
import DetailArticle from './Screens/DetailArticle'

const Navigator = createBottomTabNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions: {
                tabBarLabel: 'Home',
                tabBarIcon: props => (
                    <Icon
                        style={iconStyles(props)}
                        type="Ionicons"
                        name="ios-home"
                    />
                )
            }
        },
        Product: {
            screen: createStackNavigator(
                {
                    Product,
                    DetailProduct
                },
                {
                    headerMode: 'none',
                    initialRouteName: 'Product',
                    transitionConfig: () => fromRight()
                }
            ),
            navigationOptions: {
                tabBarLabel: 'Products',
                tabBarIcon: props => (
                    <Icon
                        style={iconStyles(props)}
                        type="Ionicons"
                        name="ios-archive"
                    />
                )
            }
        },
        Article: {
            screen: createStackNavigator(
                {
                    Article,
                    DetailArticle
                },
                {
                    headerMode: 'none',
                    initialRouteName: 'Article',
                    transitionConfig: () => fromRight()
                }
            ),
            navigationOptions: {
                tabBarLabel: 'Articles',
                tabBarIcon: props => (
                    <Icon
                        style={iconStyles(props)}
                        type="Ionicons"
                        name="ios-paper"
                    />
                )
            }
        },
        About: {
            screen: About,
            navigationOptions: {
                tabBarLabel: 'About Us',
                tabBarIcon: props => (
                    <Icon
                        style={iconStyles(props)}
                        type="Ionicons"
                        name="ios-business"
                    />
                )
            }
        }
    },
    {
        initialRouteName: 'Home',
        tabBarOptions: {
            activeTintColor: Color.Primary,
            inactiveTintColor: '#999',
            style: {
                padding: 5
            }
        }
    }
)

const iconStyles = ({ tintColor, focused }) => ({
    color: tintColor,
    fontSize: focused ? 24 : 20
})

export default createAppContainer(Navigator)
