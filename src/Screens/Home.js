import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { StyleSheet, Image } from 'react-native'
import {
    View,
    Text,
    Content,
    Thumbnail,
    Button,
    Card,
    CardItem,
    Body,
    Icon
} from 'native-base'
import { WebView } from 'react-native-webview'
import { getProducts } from '../Redux/Actions/Product'
import { getArticles } from '../Redux/Actions/Article'
import Header from '../Components/Header'
import Carousel from '../Components/Carousel'
import Loader from '../Components/Loader'
import Color from '../Assets/Color'

const Product = ({ image, name }) => (
    <View style={styles.productCol}>
        <View style={styles.product}>
            <Thumbnail
                square
                style={{ width: '100%', height: 150 }}
                source={image}
            />
            <Text
                style={{
                    marginTop: 5,
                    fontSize: 12,
                    fontWeight: 'bold',
                    textAlign: 'center'
                }}
            >
                {name}
            </Text>
        </View>
    </View>
)

const Article = ({ article }) => (
    <Card style={{ width: '100%' }}>
        <CardItem>
            <Body>
                <Image
                    source={{ uri: article.urlToImage }}
                    style={{ width: '100%', height: 200 }}
                />
                <Text style={{ fontWeight: 'bold', marginVertical: 10 }}>
                    {article.title}
                </Text>
                <Text>{article.description}</Text>
            </Body>
        </CardItem>
    </Card>
)

export default ({ navigation }) => {
    const dispatch = useDispatch()
    const { product, article } = useSelector(state => state)

    const productsHighlight = product.data.filter((item, idx) => idx < 3)
    const recentArticles = article.data.filter((item, idx) => idx < 4)

    const fetch = () => {
        dispatch(getProducts())
        dispatch(getArticles())
    }

    useEffect(() => {
        fetch()
    }, [])

    return (
        <>
            <Header
                title="Home"
                rightComponent={
                    <Button
                        transparent
                        disabled={product.loading || article.loading}
                        onPress={fetch}
                    >
                        <Icon name="refresh" />
                    </Button>
                }
            />
            <Content>
                <Carousel
                    images={recentArticles.map(article => article.urlToImage)}
                />
                <View style={styles.highlightWrapper}>
                    <Text style={styles.title}>Products Hightlight</Text>
                    <View style={styles.hr} />
                    {product.loading ? (
                        <Loader />
                    ) : (
                        <View style={styles.productWrapper}>
                            {productsHighlight.map(product => (
                                <Product
                                    key={product.id}
                                    name={product.name}
                                    image={{
                                        uri: `https://sutanlab-backend-pos.herokuapp.com/files/image/product/${
                                            product.image
                                        }`
                                    }}
                                />
                            ))}
                        </View>
                    )}
                    <Button
                        block
                        onPress={() => navigation.navigate('Product')}
                        style={{
                            marginTop: 10,
                            alignSelf: 'center',
                            backgroundColor: Color.Primary
                        }}
                    >
                        <Text>View More Products</Text>
                    </Button>
                </View>
                <View style={styles.recentWrapper}>
                    <Text style={styles.title}>Recent Articles</Text>
                    <View style={styles.hr} />
                    <View style={{ width: '100%' }}>
                        {article.loading ? (
                            <Loader />
                        ) : (
                            recentArticles
                                .filter((item, idx) => idx < 2)
                                .map((article, idx) => (
                                    <Article key={idx} article={article} />
                                ))
                        )}
                    </View>
                    <Button
                        block
                        onPress={() => navigation.navigate('Article')}
                        style={{
                            marginVertical: 10,
                            alignSelf: 'center',
                            backgroundColor: Color.Primary
                        }}
                    >
                        <Text>View More Articles</Text>
                    </Button>
                </View>
                <View style={styles.contactWrapper}>
                    <Text style={styles.title}>Contact Us</Text>
                    <View style={styles.hr} />
                    <Text style={{ textAlign: 'center' }}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserunt mollit
                        anim id est laborum.
                    </Text>
                    <View
                        style={{
                            height: 200,
                            width: '100%',
                            marginVertical: 10
                        }}
                    >
                        <WebView
                            source={{
                                html:
                                    '<html><body><iframe style="width: 100%; height: 100%" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d988.3314182737059!2d110.3954634!3d-7.7552416!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbd02b1c307eba617!2sPT%20Erporate%20Solusi%20Global!5e0!3m2!1sid!2sid!4v1574399385703!5m2!1sid!2sid"></iframe></body></html>'
                            }}
                        />
                    </View>
                </View>
            </Content>
        </>
    )
}

const styles = StyleSheet.create({
    highlightWrapper: {
        alignItems: 'center',
        marginVertical: 5
    },
    contactWrapper: {
        alignItems: 'center',
        marginVertical: 5,
        padding: 10
    },
    recentWrapper: {
        alignItems: 'center',
        marginVertical: 5,
        padding: 10
    },
    title: {
        fontSize: 22,
        fontWeight: 'bold'
    },
    hr: {
        height: 3,
        width: '40%',
        backgroundColor: '#ddd',
        marginVertical: 5
    },
    productWrapper: {
        flexDirection: 'row',
        width: '100%',
        height: 'auto',
        flexWrap: 'wrap',
        padding: 5
    },
    product: {
        backgroundColor: '#fff',
        alignItems: 'center'
    },
    productCol: {
        width: '33.3333333333%',
        paddingHorizontal: 5,
        paddingVertical: 10
    }
})
