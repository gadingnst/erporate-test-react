import React from 'react'
import { StyleSheet, Image, Linking } from 'react-native'
import { View, Text, Button, Icon, Content } from 'native-base'
import Header from '../Components/Header'
import Color from '../Assets/Color'

export default ({ navigation }) => {
    const data = navigation.getParam('article')
    return (
        <>
            <Header
                title={data.title}
                leftComponent={
                    <Button transparent onPress={() => navigation.goBack()}>
                        <Icon name="arrow-back" />
                    </Button>
                }
            />
            <Content padder>
                <Image style={styles.image} source={{ uri: data.urlToImage }} />
                <Text style={styles.title}>{data.title}</Text>
                <View style={styles.line} />
                <Text style={styles.content}>{data.content}</Text>
                <Button
                    block
                    onPress={() => Linking.openURL(data.url)}
                    style={styles.readMore}
                >
                    <Text>Read More</Text>
                </Button>
            </Content>
        </>
    )
}

const styles = StyleSheet.create({
    title: {
        fontSize: 22,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    line: {
        width: '85%',
        height: 5,
        marginVertical: 15,
        backgroundColor: '#ddd',
        alignSelf: 'center'
    },
    image: {
        width: '100%',
        height: 200,
        resizeMode: 'contain'
    },
    readMore: {
        backgroundColor: Color.Primary,
        marginTop: 20
    }
})
