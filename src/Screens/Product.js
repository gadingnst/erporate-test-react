import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { StyleSheet } from 'react-native'
import {
    Content,
    Text,
    Card,
    CardItem,
    Left,
    Thumbnail,
    Body,
    Button,
    View,
    Icon
} from 'native-base'
import { getProducts } from '../Redux/Actions/Product'
import Header from '../Components/Header'
import Loader from '../Components/Loader'
import Color from '../Assets/Color'

const CardProduct = ({ data, navigate = () => false }) =>
    data.map((product, idx) => (
        <Card key={idx}>
            <CardItem>
                <Left>
                    <Thumbnail
                        square
                        style={styles.image}
                        source={{
                            uri: `https://sutanlab-backend-pos.herokuapp.com/files/image/product/${
                                product.image
                            }`
                        }}
                    />
                    <Body>
                        <Text style={styles.title}>{product.name}</Text>
                        <View style={styles.btnWrapper}>
                            <Button
                                block
                                onPress={() =>
                                    navigate('DetailProduct', { product })
                                }
                                style={{
                                    backgroundColor: Color.Primary
                                }}
                            >
                                <Text>Detail</Text>
                            </Button>
                        </View>
                    </Body>
                </Left>
            </CardItem>
        </Card>
    ))

export default ({ navigation }) => {
    const dispatch = useDispatch()
    const { data: products, loading } = useSelector(({ product }) => product)
    const fetchProducts = () => dispatch(getProducts())

    return (
        <>
            <Header
                title="Products"
                rightComponent={
                    <Button
                        transparent
                        disabled={loading}
                        onPress={fetchProducts}
                    >
                        <Icon name="refresh" />
                    </Button>
                }
            />
            <Content padder>
                {loading ? (
                    <Loader />
                ) : (
                    <CardProduct
                        data={products}
                        navigate={navigation.navigate}
                    />
                )}
            </Content>
        </>
    )
}

const styles = StyleSheet.create({
    image: {
        borderRadius: 5,
        width: 100,
        height: 125
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16
    },
    btnWrapper: {
        flex: 1,
        flexDirection: 'column-reverse'
    }
})
