# Erporate Frontend Test

## Table of contents
- [Erporate Frontend Test](#erporate-frontend-test)
  - [Table of contents](#table-of-contents)
  - [Stack yang digunakan](#stack-yang-digunakan)
  - [API yang digunakan](#api-yang-digunakan)
  - [Hasil](#hasil)
    - [Hightlight Products](#hightlight-products)
    - [Recent Articles](#recent-articles)
    - [Contact Us](#contact-us)
    - [Products](#products)
    - [Articles](#articles)
    - [About](#about)
    - [Article Detail](#article-detail)
    - [Product Detail](#product-detail)

## Stack yang digunakan
- React Native
- Native Base
- Redux

## API yang digunakan
- Products -> https://sutanlab-backend-pos.herokuapp.com
- Articles -> https://newsapi.org/

## Hasil
> link build APK : https://drive.google.com/file/d/1m9J5U052zQFjx7V76a0w0QuYeYLMmfTw/view?usp=sharing

### Hightlight Products
[![Highlight Products](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/home_product.jpeg)](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/home_product.jpeg)

### Recent Articles
[![Recent Articles](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/home_articles.jpeg)](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/home_articles.jpeg)

### Contact Us
[![Contact Us](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/home_contact.jpeg)](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/home_contact.jpeg)

### Products
[![Recent Articles](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/product.jpeg)](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/product.jpeg)

### Articles
[![Articles](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/article.jpeg)](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/article.jpeg)

### About
[![About](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/about.jpeg)](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/about.jpeg)

### Article Detail
[![Article Detail](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/article_detail.jpeg)](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/article_detail.jpeg)

### Product Detail
[![Product Detail](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/product_detail.jpeg)](https://gitlab.com/sutanlab/erporate-test-react/raw/master/screenshots/product_detail.jpeg)